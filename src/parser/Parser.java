package parser;

import java.io.*;
import java.util.HashMap;

class Parser {
    static final String QUOTE = "\"";

    HashMap<Character,Character> charMap;
    File testFile;
    Logger logger = new Logger();

    public Parser(String testFile) {
        this.testFile = new File(testFile);
    }

    /**
     * method to create a map of open and close chars
     */
    private void makeCharMap() {
        charMap = new HashMap<Character,Character>();
        charMap.put('(', ')');
        charMap.put('{', '}');
        charMap.put('[', ']');
    }

    /**
     * method to validate a file of strings and log results
     */
    private void validateStrings() {
        makeCharMap();

        logger.log("Testing file: " + testFile.getAbsolutePath());
        try (BufferedReader br = new BufferedReader(new FileReader(testFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String testString = line.trim();
                try {
                    validateString(testString);
                    logger.log(QUOTE + testString + QUOTE + " is VALID");
                }
                catch (ValidatorException ve) {
                    logger.log(QUOTE + testString + QUOTE + " is INVALID - " + ve.getMessage());
                }
            }
        }
        catch (IOException ioe) {
            logger.error(ioe.toString());
        }
    }

    /**
     * method to validate a string
     *
     * @param testString string to test
     * @throws ValidatorException
     */
    private void validateString(String testString) throws ValidatorException {
        char[] testChars = testString.toCharArray();
        findSpecial(testChars);
    }

    /**
     * method to search an array for a set of special characters
     *
     * @param testChars array of characters to test
     * @return index of found special character or end of array
     * @throws ValidatorException
     */
    private int findSpecial(char[] testChars) throws ValidatorException {
        return findSpecial(testChars, 0, null);
    }

    /**
     * method to search an array for a set of special characters and a specified character
     * starting at a specified index
     *
     * @param testChars array of characters to test
     * @param index index to start looking
     * @param specialChar character to look for
     * @return index of found specified character or end of array
     * @throws ValidatorException
     */
    private int findSpecial(char[] testChars, int index, Character specialChar) throws ValidatorException {
        for (int i = index; i < testChars.length; i++) {
            char testChar = testChars[i];
            if (specialChar != null && specialChar.charValue() == testChar) {
                return i;
            }
            else if (charMap.containsKey(testChar)) {
                i = findSpecial(testChars, i + 1, new Character(charMap.get(testChar)));
            }
            else if (charMap.containsValue(testChar)) {
                throw new ValidatorException("Found invalid character: " + testChar);
            }
        }
        if (specialChar != null) {
            throw new ValidatorException("Reached end of string");
        }
        return testChars.length;
    }

//    *** leaving this original implementation for reference ***
//
//    private void findOpen(char[] testChars) throws ValidatorException {
//        for (int i = 0; i < testChars.length; i++) {
//            char testChar = testChars[i];
//            if (charMap.containsKey(testChar)) {
//                i = findClose(testChars, charMap.get(testChar), i + 1);
//            }
//            else if (charMap.containsValue(testChar)) {
//                throw new ValidatorException("Found invalid character: " + testChar);
//            }
//        }
//    }
//
//    private int findClose(char[] testChars, char closeChar, int index) throws ValidatorException {
//        for (int i = index; i < testChars.length; i++) {
//            char testChar = testChars[i];
//            if (closeChar == testChar) {
//                return i;
//            }
//            else if (charMap.containsKey(testChar)) {
//                i = findClose(testChars, charMap.get(testChar), i + 1);
//            }
//            else if (charMap.containsValue(testChar)) {
//                throw new ValidatorException("Found invalid character: " + testChar);
//            }
//        }
//        throw new ValidatorException("Reached end of string");
//    }

    public static void main(String[] args) {
        new Parser("src/parser/resources/data.txt").validateStrings();
    }
}

